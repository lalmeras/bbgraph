# Django settings for bbgraph project.
import os
import random
import string

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

DATABASE_ENGINE = os.getenv('DATABASE_ENGINE', 'sqlite3')    # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'ado_mssql'.
DATABASE_NAME = os.getenv('DATABASE_NAME')                   # Or path to database file if using sqlite3.
DATABASE_USER = os.getenv('DATABASE_USER', '')               # Not used with sqlite3.
DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD', '')       # Not used with sqlite3.
DATABASE_HOST = os.getenv('DATABASE_HOST', '')               # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = os.getenv('DATABASE_PORT', '')               # Set to empty string for default. Not used with sqlite3.

# Local time zone for this installation. Choices can be found here:
# http://www.postgresql.org/docs/8.1/static/datetime-keywords.html#DATETIME-TIMEZONE-SET-TABLE
# although not all variations may be possible on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = os.getenv('TZ', 'America/Chicago')

# Language code for this installation. All choices can be found here:
# http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
# http://blogs.law.harvard.edu/tech/stories/storyReader$15
LANGUAGE_CODE = os.getenv('LANGUAGE_CODE', 'en-us')

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT.
# Example: "http://media.lawrence.com"
MEDIA_URL = ''

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
random_pass = ''.join(random.choice(string.letters + string.digits + string.punctuation) for _ in range(30))
SECRET_KEY = os.getenv('SECRET_KEY', random_pass)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
#     'django.template.loaders.eggs.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.doc.XViewMiddleware',
)

ROOT_URLCONF = 'bbgraph.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'bbgraph.graph',
)

STATIC_ROOT = os.getenv('STATIC_ROOT')

BBGRAPH_IMAGE_TRANSPARENT = False
BBGRAPH_JPS2PS = os.getenv('BBGRAPH_JPS2PS')
BBGRAPH_LATEX_HOME = os.getenv('LATEX_HOME', '/tmp/')
CACHE_BACKEND = os.getenv('CACHE_BACKEND')

