from django import template

register = template.Library()

def latexize(value):
    if value is None:
        return None
    return value.replace("_", "\\_")
latexize.is_safe = True
register.filter(latexize)
