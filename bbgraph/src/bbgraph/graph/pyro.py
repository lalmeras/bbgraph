import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings_dev'

import Pyro.naming
import Pyro.core
from Pyro.errors import PyroError, NamingError

from bbgraph.graph.log import logging
from bbgraph.graph.models import Graph
from bbgraph.graph.cache import cache_eps, get_eps
from bbgraph.graph.jpswrapper import process_jps
from bbgraph.graph.graphic import graph_to_png

logger = logging.getLogger("bbgraph.pyro")

class Cache(Pyro.core.ObjBase):
    def start_caching(self):
        try:
            graphs = Graph.objects.all()
            for graph in graphs:
                logger.debug("check cache for {0}".format(graph.id))
                graph_to_png(graph, 200)
        except Exception, e:
            logger.error(e, exc_info=True)

def main():
    Pyro.core.initServer()
    daemon = Pyro.core.Daemon()
    uri = daemon.connect(Cache(), "Cache")
    daemon.requestLoop()
