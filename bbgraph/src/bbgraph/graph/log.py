import sys
import logging
from django.conf import settings

logger = logging.getLogger()
hdlr = logging.StreamHandler(sys.stderr)
formatter = logging.Formatter('[%(asctime)s] %(levelname)-6s %(name)-18s %(message)s','%Y-%m-%d %a %H:%M:%S') 

hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)

