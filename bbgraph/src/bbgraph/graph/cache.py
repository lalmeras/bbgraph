from django.core.cache import cache
from bbgraph.graph.log import logging

import unicodedata

logger = logging.getLogger('bbgraph.cache')

def _eps_cache_key(jps):
    if jps is not None:
        key = u"eps_{jps}".format(jps=jps)
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("generated key (eps) for {jps}...".format(jps=jps[0:10]))
        return key


def _png_cache_key(graph, size):
    if graph.id is not None:
        key = "png_{size}_{id}_{date}".format(size=size, id=graph.id, date=str(graph.saved_on))
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("generated key (png) for {graph} {size}: {key}".format(graph=graph, size=size, key=key))
        return key

def cache_png(graph, size, data):
    key = _png_cache_key(graph, size)
    size = len(data)
    cache.set(key, data)
    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("caching png (size: {size}) for key {key}".format(key=key, size=size))

def cache_eps(jps, data):
    key = _eps_cache_key(jps)
    size = len(data)
    cache.set(key, data)
    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("caching eps (size: {size}) for key {key}...".format(key=key[0:10], size=size))

def get_png(graph, size):
    key = _png_cache_key(graph, size)
    data = cache.get(key)
    if logger.isEnabledFor(logging.DEBUG):
        if data is None:
            logger.debug("png cache miss for {key}".format(key=key))
        else:
            logger.debug("png cache success for {key}".format(key=key))
    return data

def get_eps(jps):
    key = _eps_cache_key(jps)
    data = cache.get(key)
    if logger.isEnabledFor(logging.DEBUG):
        if data is None:
            logger.debug("eps cache miss for {key}...".format(key=key[0:10]))
        else:
            logger.debug("eps cache success for {key}...".format(key=key[0:10]))
    return data
