import os
import math

from PIL import Image

from django.conf import settings

from bbgraph.graph.cache import get_eps, cache_eps, cache_png, get_png
from bbgraph.graph.jpswrapper import process_jps

from bbgraph.graph.log import logging

logger = logging.getLogger("bbgraph.image")

def jps_to_eps(jps):
    data = get_eps(jps)
    if data is None:
        eps = os.tmpfile()
        process_jps(jps, eps)
        eps.seek(0)
        cache_eps(jps, eps.read())
        eps.seek(0)
        data = eps.read()
    return data

def graph_to_png(graph, size):
    makeTransparent = True
    if graph.id is not None:
        jpsImage = get_png(graph, size)
        if jpsImage is not None:
            return jpsImage
    eps = os.tmpfile()
    eps.write(jps_to_eps(graph.jps))
    eps.seek(0)
    im = Image.open(eps)
    logger.debug("eps image size: {0[0]}, {0[1]}".format(im.size))
    if makeTransparent and settings.BBGRAPH_IMAGE_TRANSPARENT:
        im = im.convert('RGB')
        pix = im.load()
        for i in range(im.size[0] - 1):
            for j in range(im.size[1] - 1):
                if pix[i,j][0] == 255 and pix[i,j][1] == 255 and pix[i,j][2] == 255:
                    pix[i,j] = (245,245,245)
                else:
                    pix[i,j] = (pix[i,j][0] + 100, pix[i,j][1] + 100, pix[i,j][2] + 100)
    if size is not None:
        target_size = get_size_keep_aspect_ratio(size, im.size)
        if size > im.size[0]:
            im = im.resize(target_size, Image.BICUBIC)
        else:
            im = im.resize(target_size, Image.ANTIALIAS)
    if target_size[0] != size or target_size[1] != size:
        x1 = int(math.floor(float(size - target_size[0]) / 2))
        y1 = int(math.floor(float(size - target_size[1]) / 2))
        im2 = Image.new("RGB", (size, size), (255, 255, 255, 0))
        im2.paste(im, (x1, y1))
        im = im2
    resultat = os.tmpfile()
    im.save(resultat, "PNG")
    resultat.seek(0)
    data = resultat.read()
    if graph.id is not None:
        cache_png(graph, size, data)
    return data

def get_size_keep_aspect_ratio(target, source):
    logger.debug("compute resize tuple for ({0[0]}, {0[1]}) -> {1}".format(source, target))
    if source[0] == source[1]:
        result = (target, target)
    else:
        ratio = float(source[0]) / float(source[1])
        logger.debug("non square aspect ratio {0}".format(ratio))
        if source[0] > source[1]:
            result = (target, int(math.floor(float(target) / ratio)))
        else:
            result = (int(math.floor(float(target) * ratio)), target)
    logger.debug("result tuple  ({0[0]}, {0[1]})".format(result))
    return result
