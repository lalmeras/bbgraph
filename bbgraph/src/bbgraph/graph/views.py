# -*- encoding: utf-8 -*-

from bbgraph.graph.log import logging
import shutil
import unicodedata

from django import forms
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.conf import settings
from django.http import HttpResponseServerError
from django.core import serializers

from bbgraph.graph.models import Graph, ArchivedGraph
from bbgraph.graph.jpswrapper import process_jps
from bbgraph.graph.cache import *
from bbgraph.graph.graphic import graph_to_png

import Pyro.core

try:
    caching = Pyro.core.getProxyForURI("PYROLOC://localhost:7766/Cache")
    caching._setOneway(('start_caching',))
    caching.start_caching()
except:
    pass

class JPSForm(forms.Form):
    """
    JPS form described via Django form API
    """

    """
    id of the graph, not required for not saved graph
    """
    id = forms.CharField(required=False)
    name = forms.CharField(max_length=255)
    jps = forms.CharField()

def _graph_image(graph, size='200', makeTransparent=False):
    """
    return an HttpResponse providing preview thumbnail image
    for the queried graph. graph does not need to be saved
    """
    size = int(size)
    key = None
    jps = graph.jps
    response = HttpResponse(mimetype='image/png')
    data = graph_to_png(graph, size)
    response.write(data)
    return response

def _graph_download(jps, graph):
    """
    returns an HttpResponse providing the .eps file transformation
    of the given jps
    """
    response = HttpResponse(mimetype='application/x-eps')
    if graph is None:
        response['Content-Disposition'] = 'attachment; filename=output.eps'
    else:
        name = unicodedata.normalize('NFKD', graph.name).encode('ascii', 'ignore')
        response['Content-Disposition'] = 'attachment; filename=%s.eps' % name
    process_jps(jps, response)
    return response

def graphs_backup(request):
    """
    returns an HttpResponse containing an sqlite database dump
    raise an HttpResponseServerError if DATABASE_ENGINE is not sqlite3
    """
    response = HttpResponse(mimetype='application/octet-stream')
    response['Content-Disposition'] = 'attachment; filename=backup.dat'
    if DATABASE_ENGINE == 'sqlite3':
        backup = open(settings.DATABASE_NAME)
        shutil.copyfileobj(backup, response)
        return response
    else:
        raise HttpResponseServerError()

def graphs_export(request):
    """
    returns an HttpResponse providing a database dump in text format
    """
    response = HttpResponse(mimetype='text/plain')
    graphs = Graph.objects.all()
    for graph in graphs:
        response.write("\r\n%s\r\n#################\r\n%s\r\n#################\r\n\r\n\r\n" % (graph.name, graph.jps))
    return response

def _graph_edit(id):
    """
    provides template response for editing a graph
    id is the id of the graph to edit, else None for a new graph
    """
    graphs = Graph.objects.all()
    if id is not None:
        curgraph = Graph.objects.get(id=id)
    else:
        curgraph = Graph()
    template_dict = {
            'graphs':   graphs,
            'curgraph': curgraph,
            }
    return render_to_response("graph.html", template_dict)

def _save_new(form):
    """
    save new graph then redirect
    """
    graph = Graph(name=form.cleaned_data['name'], jps=form.cleaned_data['jps'])
    graph.save()
    return HttpResponseRedirect("/graph/%s/" % (graph.id,))

def _save(form):
    """
    save graph described by form (new or not)
    """
    if form.cleaned_data['id'] is not None:
        graph = Graph.objects.get(id=form.cleaned_data['id'])
    else:
        return _save_new(form)
    graph.name = form.cleaned_data['name']
    graph.jps = form.cleaned_data['jps']
    graph.save()
    return HttpResponseRedirect("/graph/%s/" % (graph.id,))

def graph(request, id=None):
    """
    query dispatcher :
    POST/save (new or not)
    POST/save_copy (new)
    POST/download (not new)
    """
#    try:
    if request.method == 'POST':
        form = JPSForm(request.POST)
        if not form.is_valid():
            raise Exception(form.errors)
        if request.POST.has_key('download'):
            if request.POST['id'] is not None and request.POST['id'] != 'None':
                graph = Graph.objects.get(id=request.POST['id'])
            else:
                graph = None
            var = request.POST['jps']
            return _graph_download(var, graph)
        elif request.POST.has_key('save'):
            return _save(form)
        elif request.POST.has_key('save_copy'):
            return _save_new(form)
        else:
            pass
    else:
        return _graph_edit(id)
#    except JPSError, error:
#		return render_to_response('graph.html', {'error': error, 'graphs': Graph.objects.all()})
#    except Exception, error:
#        return render_to_response('graph.html', {'error': error, 'graphs': Graph.objects.all()})

def graph_image(request, id, size=200):
    """
    provides graph thumbnail (graph retrieved by id)
    """
    graph = get_object_or_404(Graph, pk=id)
    return _graph_image(graph, size)

def graph_preview(request, size=200):
    """
    provides graph thumbnail (jps retrieved in query)
    """
    try:
        logging.debug("test")
        jps = request.GET['jps']
        graph = Graph(jps=jps)
        if size is None:
            makeTransparent = True
            size = 400
        else:
            makeTransparent = False
        return _graph_image(graph, size, makeTransparent)
    except Exception, e:
        logging.getLogger("graph.image").exception(e)
        return HttpResponseRedirect("/static/images/error.png")

def graph_download(request):
    """
    provides graph eps (jps retrieved in query)
    """
    try:
        return _graph_download(request.GET['jps'])
    except KeyError, e:
        raise HttpResponseServerError()

def delete_graph(request, id=None):
    """
    delete graph by id
    """
    if id is not None:
        graph = Graph.objects.get(id=id)
        graph.delete()
    return HttpResponseRedirect("/graph/")

def archive(request, id=None):
    if id is not None:
         graph = ArchivedGraph.objects.get(id=id)
         template_dict = {
            'archive':   graph,
         }
         return render_to_response("archive.html", template_dict)

def json_archives(request, id):
    graph = Graph.objects.get(id=id)
    archives = graph.archivedgraph_set.all()
    return HttpResponse(serializers.serialize('json', archives, ensure_ascii=False), mimetype='application/javascript')
    #return HttpResponse(simplejson.dumps(archives), mimetype='application/javascript')

def archive_image(request, id, size=200):
    """
    provides graph thumbnail (graph retrieved by id)
    """
    graph = get_object_or_404(ArchivedGraph, pk=id)
    return _graph_image(graph, size)

def test(request):
    # TODO: to delete, temporary code
    return render_to_response("test.html")
