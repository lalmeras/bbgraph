from django.db import models
from django.db.models.signals import post_save

class Graph(models.Model):
    name        = models.CharField(max_length=255)
    jps         = models.TextField()
    template    = models.BooleanField(default=False)
    saved_on    = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']

class ArchivedGraph(models.Model):
    graph       = models.ForeignKey(Graph)
    name        = models.CharField(max_length=255)
    jps         = models.TextField()
    template    = models.BooleanField(default=False)
    version     = models.IntegerField()
    saved_on    = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name', 'version']

def archive_graph(sender, **kwarg):
    created = kwarg['created']
    instance = kwarg['instance']
    if isinstance(instance, Graph):
       last_archives = instance.archivedgraph_set.order_by('-version')
       if len(last_archives) > 0:
           version = last_archives[0].version + 1
       else:
           version = 1
       archive = ArchivedGraph(name=instance.name, jps=instance.jps, template=instance.template, version=version, graph=instance)
       archive.save()

post_save.connect(archive_graph)
