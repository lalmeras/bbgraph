# -*- encoding: utf-8 -*-

import shutil
from subprocess import call
from os import tmpfile
from django.conf import settings

JPS2PS = ['perl', settings.BBGRAPH_JPS2PS]
GS = [ "gs", "-dNODISPLAY", "-dNOPAUSE", "-dBATCH", "-dQUIET", "-" ]

class JPSError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)
    
    def __unicode__(self):
        return unicode(str(self))

def process_jps(jps, file):
    input = tmpfile()
    output = tmpfile()
    err = tmpfile()
    input.write(jps.encode("latin1"))
    input.seek(0)
    result = call(JPS2PS,
        env={ 'PATH' : '/bin:/usr/bin', 'HOME' : settings.BBGRAPH_LATEX_HOME },
        stdin=input, stdout=output, stderr=err)
    err.seek(0)
    if result != 0:
        raise JPSError, "\n".join(err.readlines())

    input = tmpfile()
    output.seek(0)
    input.write(output.read())
    input.seek(0)
    result = call(GS, stdin=input, stderr=None, stdout=None)
    if result != 0:
        raise JPSError, u"Le fichier généré n'est pas un fichier postscript valide"

    output.seek(0)
    shutil.copyfileobj(output, file)
    return

