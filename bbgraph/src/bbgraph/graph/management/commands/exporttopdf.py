# -*- encoding: utf-8 -*-

from tempfile import mkdtemp
from os.path import join, splitext
from subprocess import call
import sys

from django.core.management.base import BaseCommand
from django.template.loader import render_to_string
from django.conf import settings

from bbgraph.graph.models import Graph, ArchivedGraph
from bbgraph.graph.jpswrapper import process_jps, JPSError

LATEX_COMMAND = [ "latex" ]
DVIPS_COMMAND = [ "dvips", "-tlandscape", "-ta4", "-Ppdf" ]
PS2PDF_COMMAND = [ "ps2pdf", "-sPAPERSIZE=a4" ]

class PdfGraph(Graph):
    def __init__(self, graph, old_epsfile, new_epsfile):
        self.graph = graph
        self.old_epsfile = old_epsfile
        self.new_epsfile = new_epsfile

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        graphs = Graph.objects.all()
        path = mkdtemp('bbgraph')
        pdfgraphs = []
        for graph in graphs:
            if graph.id != 42:
                continue
            old_epsfile = join(path, str(graph.id)) + "old.eps"
            new_epsfile = join(path, str(graph.id)) + "new.eps"
            old_file = open(old_epsfile, 'w')
            new_file = open(new_epsfile, 'w')

            try:
                settings.BBGRAPH_JPS2PS = "/home/lalmeras/Documents/bbgraph/parts/jps2ps016/jps2ps.pl"
                process_jps(graph.jps, old_file)
                print "%s: %s" % (old_epsfile, graph.name.encode('utf-8'))
            except Exception, e:
                sys.stderr.write(u"Erreur sur le graphique n %d" % graph.id)
                old_epsfile = None
            finally:
                old_file.close()
            try:
                settings.BBGRAPH_JPS2PS = "/home/lalmeras/Documents/bbgraph/parts/jps2ps/jps2ps.pl"
                process_jps(graph.jps, new_file)
                print "%s: %s" % (new_epsfile, graph.name.encode('utf-8'))
            except:
                sys.stderr.write(u"Erreur sur le graphique n %d" % graph.id)
                new_epsfile = None
            finally:
                new_file.close()
            pdfgraphs.append(PdfGraph(graph, old_epsfile, new_epsfile))
        latex_content = render_to_string("pdf.latex", { "graphs" : pdfgraphs })
        latex_file = join(path, "latex.tex")
        dvi_file = splitext(latex_file)[0] + ".dvi"
        ps_file = splitext(latex_file)[0] + ".ps"
        pdf_file = splitext(latex_file)[0] + ".pdf"

        f = open(latex_file, "w")
        f.write(latex_content.encode("utf-8"))
        f.close()
        del f

        latex_command = list(LATEX_COMMAND)
        latex_command.append(latex_file)
        call(latex_command, cwd=path)

        dvips_command = list(DVIPS_COMMAND)
        dvips_command.append(dvi_file)
        call(dvips_command, cwd=path)

        ps2pdf_command = list(PS2PDF_COMMAND)
        ps2pdf_command.append(ps_file)
        call(ps2pdf_command, cwd=path)

