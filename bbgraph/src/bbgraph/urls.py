from django.conf.urls.defaults import *
from django.conf import settings

urlpatterns = patterns('',
    # Example:
    # (r'^bbgraph/', include('bbgraph.foo.urls')),

    # Uncomment this for admin:
    # (r'^admin/', include('django.contrib.admin.urls')),
    (r'^$', 'django.views.generic.simple.redirect_to', { 'url' : '/graph' }),
    (r'^graph/$', 'bbgraph.graph.views.graph'),
    (r'^graph/(?P<id>\d+)/$', 'bbgraph.graph.views.graph'),
    (r'^graph/(?P<id>\d+)/png/$', 'bbgraph.graph.views.graph_image'),
    (r'^graph/(?P<id>\d+)/png/(?P<size>\d+)/$', 'bbgraph.graph.views.graph_image'),
    (r'^graph/(?P<id>\d+)/thumbnail/$', 'bbgraph.graph.views.graph_image', {'size': 60}),
    (r'^graph/delete/(?P<id>\d+)/$', 'bbgraph.graph.views.delete_graph'),
    (r'^graph/preview/$', 'bbgraph.graph.views.graph_preview', {'size': 200}),
    (r'^graph/preview_full/$', 'bbgraph.graph.views.graph_preview', {'size': None}),
    (r'^graph/download/$', 'bbgraph.graph.views.graph_download'),
    (r'^graph/graphs_backup/$', 'bbgraph.graph.views.graphs_backup'),
    (r'^graph/graphs_export/$', 'bbgraph.graph.views.graphs_export'),
    (r'^graph/archive/(?P<id>\d+)/$', 'bbgraph.graph.views.archive'),
    (r'^graph/archive/(?P<id>\d+)/png/$', 'bbgraph.graph.views.archive_image'),
    (r'^graph/json/graph_archives/(?P<id>\d+)/$', 'bbgraph.graph.views.json_archives'),
    (r'^test/$', 'bbgraph.graph.views.test'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    )
