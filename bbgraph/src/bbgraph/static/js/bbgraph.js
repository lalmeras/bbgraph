MooTools.lang.setLanguage("fr-FR");
var BBGraph = {};

BBGraph.PreviewGraphButton = new Class({
    Implements: [Log, Options, Events],

    options: {
        loadingImage: 'loading.gif',
    },

    initialize: function(element, graphId, form, graphPreviewUrl, graphPreviewImage, options) {
        this.enableLog().log("update preview graph button initialization");
        this.element = element;
        this.graphId = graphId;
        this.form = form;
        this.graphPreviewUrl = graphPreviewUrl;
        this.graphPreviewImage = graphPreviewImage;
        this.setOptions(options);
        this.element.store('BBGraph.PreviewGraphButton', this);
        this.element.addEvent('click', this.click.bind(this));

        this.keyboardEvents = new Keyboard({
            defaultEventType: 'keydown',
            events: {
                'ctrl+q': this._updatePreviewKeyEvent.bind(this)
            },
        });
    },

    _updatePreviewKeyEvent: function(event) {
        this.log("key event %1.o", event);
        this.updatePreview();
        event.preventDefault();
    },
    
    updatePreview: function() {
        if ($chk(this.form)) {
            var queryString = this.form.toQueryString();
            Asset.image(this.options.loadingImage, this._getAssetOptions());
            Asset.image(this.graphPreviewUrl + '?' +  queryString, this._getAssetOptions());
        }
    },

    _getAssetOptions: function() {
        return {
            onload: this._updatePreviewImage.bind(this),
            onerror: this.error.bind(this),
            onabort: this.abort.bind(this),
        };
    },

    click: function(event) {
        this.updatePreview();
        event.preventDefault();
    },

    error: function(image) {
        this.log("error downloading requested image %1.o", image);
        this.fireEvent("error", [ image ]);
    },

    abort: function(image) {
        this.log("abort downloading image %1.o", image);
        this.fireEvent("abort", [ image ]);
    },

    _updatePreviewImage: function(image) {
        if ($chk(this.graphPreviewImage)) {
            this.graphPreviewImage.empty();
            this.graphPreviewImage.grab(image);
        }
    },
});

BBGraph.RefreshThumbnails = new Class({
    Implements: [ Log, Chain ],

    initialize: function() {
        this.enableLog().log("refresh thumbnail initialization");
        this.timer = null;
    },

    refresh: function() {
        if ($chk(this.timer)) {
            $clear(this.timer);
        }
        this.timer = this._refresh.delay(1500, this);
    },

    _refresh: function() {
        var graphsList = $('graphs_list');
        var height = graphsList.getSize().y;
        var scrollStart = graphsList.getScroll().y;
        $$('.graph_item').each(function(item, index, items) {
            if (item.getPosition(graphsList).y + 60 > scrollStart
                    && item.getPosition(graphsList).y < height + scrollStart) {
                this.chain(function() {
                    var url = item.getElement('.bbgraph_js div[title=thumbnailUrl]').get('text');
                    var graphId = item.getElement('.bbgraph_js div[title=graphId]').get('text');
                    var image = new Asset.image(url, { onload: function(img) {
                        img.replaces(this.target.getElement('.thumbnail_preview'));
                        img.addEvent('click', function() {
                            var mask = new BBGraph.Carousel(document.body);
                            mask.setGraphId(graphId);
                            mask.show();
                        });
                    }, 'class': 'thumbnail_preview', });
                    image.target = item;
                } (item, index, items));
            }
        }, this);
    },
});

BBGraph.Carousel = new Class({
    Extends: Mask,

    Implements: Log,

    initialize: function(element, options) {
        options = $merge({
            hideOnClick: true,
            onHide: this.destroy,
        }, options);
        this.parent(element, options);
        this.enableLog();
        this.graphId = null;
        this.content = new Element('div', {'class': 'mask-content'});
        this._imageWrapper = new Element('div', {'class': 'image-wrapper'});
        this._archiveWrapper = new Element('div', {'class': 'archive-wrapper'});
        this._imageWrapper.inject(this.content, 'top');
        this._archiveWrapper.inject(this.content, 'top');
        this.content.inject(this.element, 'top');
        this.addEvent('show', this._loadGraph);
    },

    setGraphId: function(graphId) {
        this.graphId = graphId;
    },

    show: function() {
        if ($chk(this.graphId)) {
            this.parent();
        }
    },

    _loadGraph: function() {
        this._imageWrapper.empty();
        var loader = new Element('img', { src: '/static/images/loading.gif' });
        loader.inject(this._imageWrapper, 'top');
        var size = this.element.getSize();
        var minSize = Math.min(size.x, size.y) - 100;
        var url = '/graph/' + this.graphId + '/png/' + minSize + '/';
        this.log('load %s image', url);
        var graphImg = new Asset.image(url, { onerror: this.log, onload: this._inject.bind(this) });
        var request = new Request.JSON({
            url: '/graph/json/graph_archives/' + this.graphId + '/',
            onSuccess: this._displayArchives.bind(this),
            onFailure: console.dir,
            onException: console.dir,
        });
        request.send();
    },

    _displayArchives: function(data) {
        var archives = new Element('div', { 'class': 'archive_list' });
        var ul = new Element('ul');
        ul.inject(archives, 'top');
        this.log('nombre d\'éléments dans l\'historique : %d', data.length);
        $A(data).each(function(item, index, items) {
            var graph = new BBGraph.Graph(item);
            var li = new Element('li');
            li.set('text', item.fields.saved_on.format("%A %d %B %Y"));
            li.inject(ul, 'bottom');
        });
        this._archiveWrapper.empty();
        archives.inject(this._archiveWrapper, 'bottom');
    },

    _inject: function(element) {
        this.log('injection pour le graphique %s', this.graphId);
        this._imageWrapper.empty();
        element.inject(this._imageWrapper, 'top');
    },
});

BBGraph.MyDate = new Class({
    Implements: Options,

    options: {
        pattern: /(\d{4})-(\d{2})-(\d{2}).*/,
        year: 1,
        month: 2,
        day: 3,
    },

    initialize: function(options) {
        this.setOptions(options);
    },

    toDate: function(value) {
        var found = value.match(this.options.pattern);
        return new Date(
            found[this.options.year],
            found[this.options.month],
            found[this.options.day]
        );
    },
});

BBGraph.Graph = new Class({
    initialize: function(jsonObject) {
        $extend(this, jsonObject);
        var mydate = new BBGraph.MyDate().toDate(jsonObject.fields.saved_on);
        this.fields.saved_on = mydate;
    },
});

BBGraph.AutoExtend = new Class({
    Implements: Log,

    initialize: function(element) {
        this.target = document.id(element);
        window.addEvent('domready', this._extendsDiv.bind(this));
        window.addEvent('resize', this._resize.bind(this));
    },

    _resize: function() {
        $clear(this._timer);

        this._timer = this._extendsDiv.bind(this).delay(500);
    },

    _extendsDiv: function() {
        this.enableLog();
        var size = window.getSize();
        this.log('detected viewport size : %1.d x %2.d', size.x, size.y);

        //this.target.setStyle('position', 'absolute');
        //this.target.setStyle('top', 0);
        //this.target.setStyle('width', size.x);
        this.target.setStyle('height', size.y);

        var lastChild = this.target.getLast();
        if (lastChild) {
            this.log('found last child of %1.x : %2.x', this, lastChild);
            var coordinates = lastChild.getCoordinates(window);
            var bottom = this.target.getCoordinates(window);
            var heightSpace = bottom.bottom - coordinates.bottom;
            this.log('detected %dpx remaining space', heightSpace);

            var extends_ = this.target.getElements('.extend');
            extends_.each(function(extend, index, items) {
                var currentHeight = extend.getSize().y;
                var newHeight = currentHeight + heightSpace.toInt();
                this.log('resizing %o from %d to %d', extend, currentHeight, newHeight);
                extend.setStyle('height', newHeight);
            }, this);
        } else {
            this.log('empty element, no last child for %1.x', this);
            return;
        }
    },
});

