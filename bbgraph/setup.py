from setuptools import setup, find_packages
import sys, os

version = '0.2.4'

setup(name='bbgraph',
      version=version,
      description="bbgraph",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='',
      author='Laurent Almeras',
      author_email='lalmeras@gmail.com',
      url='',
      license='',
      package_dir={'': 'src'},
      packages=find_packages('src', exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          # -*- Extra requirements: -*
          'Django==1.2.3',
          'Pillow==4.0.0',
          'Pyro<4.0'
      ],
      entry_points="""
      # -*- Entry points: -*-
      [console_scripts]
      pyro-daemon = bbgraph.graph.pyro:main
      """,
      )
