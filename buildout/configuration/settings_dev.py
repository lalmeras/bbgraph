# Django settings for bbgraph project.
import os
import django
import bbgraph
# calculated paths for django and the site
# used as starting points for various other paths
DJANGO_ROOT = os.path.dirname(os.path.realpath(django.__file__))
BBGRAPH_ROOT = os.path.dirname(os.path.realpath(bbgraph.__file__))
SITE_ROOT = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

#DEBUG = True
#TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'NAME': os.path.join(SITE_ROOT, 'data', 'bbgraph-0.2.4.sqlite'),
        'ENGINE': 'sqlite3',
        'USER': '',
        'PASSWORD': '',
    }
}


# Local time zone for this installation. Choices can be found here:
# http://www.postgresql.org/docs/8.1/static/datetime-keywords.html#DATETIME-TIMEZONE-SET-TABLE
# although not all variations may be possible on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
# http://blogs.law.harvard.edu/tech/stories/storyReader$15
LANGUAGE_CODE = 'fr'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT.
# Example: "http://media.lawrence.com"
MEDIA_URL = ''

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'i8os14=2&2p1ntn5%t3=(5kvlfn88d48nz^)=scb9g%!gvv(3_'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
#     'django.template.loaders.eggs.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.doc.XViewMiddleware',
)

ROOT_URLCONF = 'bbgraph.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    '/home/lalmeras/bbgraph/v0.2.2/src/web/templates',
    '/home/lalmeras/bbgraph/dev/django-dist/lib/python/django/contrib/admin/templates',
    '/home/lalmeras/bbgraph/dev/django-dist/lib/python/django/contrib/admin/templates/admin'
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'bbgraph.graph',
)

STATIC_ROOT = os.path.join(BBGRAPH_ROOT, 'static')

BBGRAPH_IMAGE_TRANSPARENT = False
BBGRAPH_JPS2PS = '/home/lalmeras/Documents/bbgraph/parts/jps2ps016/jps2ps.pl'
BBGRAPH_LATEX_HOME = os.environ['HOME']
CACHE_BACKEND = 'file://' + SITE_ROOT + '/cache?timeout=6000&max_entries=1500'
