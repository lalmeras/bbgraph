#! /bin/bash

# rpm-build.sh 0.2.3 1 http://svn.. http://data... x86_64 fedora "-march=core2 -m64" 2.6
# rpm-build.sh 0.2.3 1 http://svn.. http://data... i686 centos "-march=i686 -m32" 2.6

version=$1
release=$2
directory=$3
findlinks=$4
arch=$5
distribution=$6
cflags=$7
pydevversion=$8

python2.6 bootstrap.py
bin/buildout

bin/buildout-source-release -n bbgraph-bundle-${version} ${directory} buildout.cfg "buildout:find-links=${findlinks}" "environment:CFLAGS=${cflags}"

mkdir -p rpmbuild/{SOURCES,SPECS,RPMS,BUILD,SRPMS}

cp bbgraph-bundle-${version}.tgz rpmbuild/SOURCES/bbgraph-bundle-${version}.tar.gz

QA_RPATHS=[0-7] setarch ${arch} rpmbuild --target ${arch}-${distribution}-linux \
    --define "${distribution} 1" \
    --define "_topdir $PWD/rpmbuild" \
    --define "_pydevversion ${pydevversion}" \
    --define "bbgraph_version ${version}" \
    --define "bbgraph_release ${release}" \
    -ba bbgraph-bundle.spec

