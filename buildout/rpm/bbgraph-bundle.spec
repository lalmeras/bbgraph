%if %{?fedora:1}%{!?fedora:0}
%define _pyver 2.6
%define _py py%{_pyver}
%define _py_devel python-devel
%endif
%if %{?centos:1}%{!?centos:0}
%define _pyver 2.6
%define _py py%{_pyver}
%define _py_devel python26-devel
%endif

Name:           bbgraph-bundle
Version:        %{bbgraph_version}
Release:        %{bbgraph_release}%{?dist}
Summary:        bbgraph

Group:          Applications/Internet
License:        GPL
URL:            http://likide.org/bbgraph
Source0:        bbgraph-bundle-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python(abi) >= %{_pyver}
BuildRequires:	%{_py_devel} >= %{_pyver}
BuildRequires:	autoconf
BuildRequires:	libstdc++-devel
Requires:       python(abi) >= %{_pyver}
Requires:	tetex-latex
Autoprov:	0
Autoreq:	0
ExclusiveArch:	i686 x86_64

%description
bbgraph is a django application to draw mathematical figures


%prep
%setup -q


%build
mkdir -p opt/bbgraph
find * -not -name opt -maxdepth 0 -exec mv {} opt/bbgraph \;
pushd opt/bbgraph
python%{_pyver} install.py "bbgraph:relative-paths=true" \
	"modwsgi:latex_home=/data/services/bbgraph/latex-home" \
	"buildout:bbgraph-configuration-path=/etc/bbgraph" \
	"PIL:rpath=/opt/bbgraph/parts/zlib/lib
		/opt/bbgraph/parts/libjpeg/lib" \
	"environment:CFLAGS=%{optflags}" \
	"zlib:environment=CFLAGS=%{optflags}
		LDFLAGS=-Wl,-rpath=/opt/bbgraph/parts/zlib/lib" \
	"libjpeg:configure-options=--prefix=/opt/bbgraph/parts/libjpeg" \
	"libjpeg:make-targets=DESTDIR=%{_builddir}/%{name}-%{version} install" \
	"libjpeg:environment=CFLAGS=%{optflags}
		LDFLAGS=-Wl,-rpath=/opt/bbgraph/parts/libjpeg/lib" \
	"ghostscript:configure-options=--prefix=/opt/bbgraph/parts/ghostscript" \
	"ghostscript:make-targets=DESTDIR=%{_builddir}/%{name}-%{version} install"
BBGRAPH_PATH=`readlink -f %{_builddir}/%{name}-%{version}/opt/bbgraph`
sed -i -e s@${BBGRAPH_PATH}@/opt/bbgraph@g \
	parts/django-bbgraph/django-bbgraph.modwsgi
popd


%install
rm -rf opt/bbgraph/patches \
	opt/bbgraph/install.py \
	opt/bbgraph/.installed.cfg \
	opt/bbgraph/release-distributions \
	opt/bbgraph/django-bbgraph.modwsgi.jinja \
	opt/bbgraph/bin/buildout-source-release \
	opt/bbgraph/buildout*.cfg \
    opt/bbgraph/bootstrap.py* \
    opt/bbgraph/configuration \
    opt/bbgraph/rpm
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
cp -ar . $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/opt/bbgraph/share/www
ln -s /opt/bbgraph/eggs/bbgraph-%{version}%{?_pydevversion:%_pydevversion}-%{_py}.egg/bbgraph/static $RPM_BUILD_ROOT/opt/bbgraph/share/www/static


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
/opt/bbgraph/develop-eggs/PIL-1.1.7-%{_py}-linux-%{_target_cpu}.egg
/opt/bbgraph/eggs/Django-1.0.4-%{_py}.egg
/opt/bbgraph/eggs/bbgraph-%{version}%{?_pydevversion:%_pydevversion}-%{_py}.egg
/opt/bbgraph/eggs/collective.recipe.patch-0.2.2-%{_py}.egg
/opt/bbgraph/eggs/gocept.download-0.9.4-%{_py}.egg
/opt/bbgraph/eggs/hexagonit.recipe.cmmi-1.3.0-%{_py}.egg
/opt/bbgraph/eggs/hexagonit.recipe.download-1.4.1-%{_py}.egg
/opt/bbgraph/eggs/setuptools-0.6c11-%{_py}.egg
/opt/bbgraph/eggs/zc.buildout-1.5.0b2-%{_py}.egg
/opt/bbgraph/eggs/zc.recipe.egg-1.2.3b2-%{_py}.egg
/opt/bbgraph/eggs/amplecode.recipe.template-1.0-%{_py}.egg
/opt/bbgraph/eggs/Jinja2-2.5-%{_py}.egg
/opt/bbgraph/eggs/buildout.dumppickedversions-0.4-%{_py}.egg
/opt/bbgraph/parts/jps2ps016
/opt/bbgraph/parts/libjpeg
/opt/bbgraph/parts/zlib
/opt/bbgraph/parts/ghostscript
/opt/bbgraph/parts/django-bbgraph/django-bbgraph.modwsgi
/opt/bbgraph/bin/django-admin
/opt/bbgraph/parts/mod_wsgi/mod_wsgi.so
/opt/bbgraph/share/www/static
/opt/bbgraph/versions.txt


%changelog
